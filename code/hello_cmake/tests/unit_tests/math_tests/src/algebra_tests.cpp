#include "gtest/gtest.h"
#include "algebra.h"


TEST(algebra, add)
{
  Algebra a;
  ASSERT_EQ(a.add(2, 3), 5);
}

TEST(algebra, subtract)
{
  Algebra a;
  ASSERT_EQ(a.subtract(7, 3), 4);
}

TEST(algebra, multiply)
{
  Algebra a;
  ASSERT_EQ(a.multiply(2, 3), 6);
}

TEST(algebra, divide)
{
  Algebra a;
  ASSERT_EQ(a.divide(6, 3), 2);
}
