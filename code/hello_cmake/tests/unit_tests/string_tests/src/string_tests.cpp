#include "gtest/gtest.h"
#include "str.h"


TEST(string, compare_true)
{
    Str s;
    EXPECT_TRUE(s.are_equal(std::string("aaa"), std::string("aaa")));
}

TEST(string, compare_false)
{
    Str s;
    EXPECT_FALSE(s.are_equal(std::string("aaa"), std::string("aaaB")));
}

TEST(string, compate_length)
{
    Str s;
    EXPECT_TRUE(s.is_longer(std::string("aaab"), std::string("aaa")));
}
