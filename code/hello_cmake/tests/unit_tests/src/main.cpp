#include "gtest/gtest.h"

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

TEST(example, dummy_pass)
{
  ASSERT_TRUE(true);
}

TEST(example, dummy_fail)
{
  ASSERT_TRUE(false);
}
