#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

TEST_CASE("add")
{
  REQUIRE(2 + 2 == 4);
}

TEST_CASE("multiply")
{
  REQUIRE(3 * 6 == 18);
}