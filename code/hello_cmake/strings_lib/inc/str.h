#ifndef STR_H
#define STR_H

#include <string>

struct Str
{
    bool are_equal(const std::string& a, const std::string& b);
    bool is_longer(const std::string& a, const std::string& b);
};

#endif
