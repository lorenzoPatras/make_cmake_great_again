#include "str.h"

bool Str::are_equal(const std::string& a, const std::string& b)
{
    return a.compare(b) == 0;
}

bool Str::is_longer(const std::string& a, const std::string& b)
{
    return a.length() > b.length();
}
