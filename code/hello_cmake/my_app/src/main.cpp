#include <iostream>

#include "hello.h"
// by making adding include directories to my_lib PUBLIC, whenever my_lib will be linked
// the linking target will add my_lib/inc path to its include paths

int main()
{
  std::cout << "hello cmake" << std::endl;
  Hello h;
  h.print();

  return 0;
}
