function(get_gtest)
    include(FetchContent)

    # declare what you want to fetch and the external resource from where to fetch it
    FetchContent_Declare(
      googletest
      GIT_REPOSITORY https://github.com/google/googletest.git
      GIT_TAG        v1.10.x
    )

    # check if the content was already populated somewhere in the cmake hierarchy before
    FetchContent_GetProperties(googletest)
    if(NOT googletest_POPULATED)
      FetchContent_Populate(googletest)
      add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})
    endif()
endfunction()
