#ifndef ALGEBRA_H
#define ALGEBRA_H

struct Algebra
{
    int add(const int a, const int b);
    int subtract(const int a, const int b);
    int multiply(const int a, const int b);
    int divide(const int a, const int b);
};

#endif
