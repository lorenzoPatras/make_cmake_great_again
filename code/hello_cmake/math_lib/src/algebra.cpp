#include "algebra.h"

int Algebra::add(const int a, const int b)
{
    return a + b;
}

int Algebra::subtract(const int a, const int b)
{
    return a - b;
}

int Algebra::multiply(const int a, const int b)
{
    return a * b;
}

int Algebra::divide(const int a, const int b)
{
    return a / b;
}
