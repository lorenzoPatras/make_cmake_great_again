= CMake basics =

== Specify minimum version ==
[source, cmake]
cmake_minimum_required(VERSION 3.11)

[.yellow]#cmake_minimum_required(VERSION 3.11)# will specify the minimum required version of CMake that should be able to build this project.

== Project name ==
[source, cmake]
project(hello_cmake VERSION 0.1
                    DESCRIPTION "hello project"
                    LANGUAGES CXX)

[.yellow]#hello_cmake# will be the name of the project. This will also populate the CMake variable PROJECT_NAME with the string "hello_cmake".

Supported languages: C, CXX (for c++), Fortran, ASM, CUDA, CSharp, SWIFT(3.15+)

== Creating executable ==
[source, cmake]
add_executable(${PROJECT_NAME} my_app/src/main.cpp)

The first argument is the name of the target while the other arguments are the files that should be build. Header files are not necessary to be added here but in case you use an IDE and you want to see the header files is better to add them here.

=== Build a simple executable ===
Checkout [.yellow]#commit 08b70fd#
[source, bash]
git checkout 08b70fd
mkdir build
cd build
cmake ../code/hello_cmake/my_app
[cmake specific output regatding compilers and generating should be displayed]
cmake --build .

NOTE: Depending on platforms the output of build might be in different folders. Search for the executable hello_cmake and run it. (On windows it will reside in a Debug folder).


== Libraries ==
[source, cmake]
add_library(${PROJECT_NAME} [STATIC, SHARED, MODULE] ${SRC})

The first argument is the name of the target, the second one can be any of _STATIC_, _SHARED_ or _MODULE_ while the third is the source files that will be included in the library.

=== Build a static lib ===
[build a static lib]
Checkout [.yellow]#commit d2e06c1#
[source, bash]
git checkout d2e06c1
mkdir build
cd build
cmake ../code/hello_cmake/my_lib
[cmake specific output regatding compilers and generating should be displayed]
cmake --build .

You should find in the build folder a static library (my_lib.lib on windows and my_lib.a on unix systems).

=== Build a dynamic lib ===
To build a dynamic lib, one can follow the above step and just change in CMakeLists.txt:
[source, cmake]
add_library(${PROJECT_NAME} SHARED src/hello.cpp)

This will result in a dynamic lib (.dll on windows, .so on linux, .dylib on mac).

== Include ==
[source, cmake]
target_include_directories(${PROJECT_NAME} PUBLIC ${PROJECT_SOURCE_DIR}/inc)

This adds a path to the target where it will look for files included. In case of executables, _PUBLIC_ is not that important while for libraries _PUBLIC_ specifies that the other targets that ling the current target will need to include those directories. Other options are _PRIVATE_ (private includes are needed only inside the target) and _INTERFACE_ (only needed for dependencies).

An example of this can be found in <<build a static lib>>.

== Add subdirectories ==
[source, cmake]
add_subdirectory(my_app)

This lets you add multiple sub projects into a big one and build all of the at once. The top level CMakeLists.txt will trigger the lower level CMakeLists.txt.

=== Build multi-level project ===
Checkout [.yellow]#commit 5565daf#
[source, bash]
git checkout 5565daf
mkdir build
cd build
cmake ../code/hello_cmake
cmake --build .

At this point you should see in the build folder both the my_app executable and my_app lib.

== Linking libraries ==
[source, cmake]
target_link_libraries(my_app PUBLIC my_lib)

This will tell my_app to link my_lib. Because in my_lib, include directories were added as _PUBLIC_, my_app will be able to _#include "hello.h"_ because it will add my_lib/inc to its include directories paths.  

=== Build multi-level project with linked targets ===
Checkout [.yellow]#commit 02abaae#
[source, bash]
git checkout 02abaae
mkdir build
cd build
cmake ../code/hello_cmake
cmake --build .

Now, if you run executable my_app, you should see both prints from my_app and my_lib.

TIP: In my_lib/CMakeLists.txt change _target_include_directories(${PROJECT_NAME} PUBLIC ${PROJECT_SOURCE_DIR}/inc)_ to _target_include_directories(${PROJECT_NAME} *PRIVATE* ${PROJECT_SOURCE_DIR}/inc)_ and repeat the above steps. The build should fail because my_app/main.cpp will not know where to look for _hello.h_.

== Bonus: aliases ==
You can rename your library by using aliases. 
[source, cmake]
add_library(lib ALIAS ${PROJECT_NAME})

This results in the fact that when you link this library you can link it by its alias:
[source, cmake]
target_link_libraries(${PROJECT_NAME} PUBLIC lib)

To see an example checkout [.yellow]#commit 048e828#

== Install ==
CMake allows you to define what do you want to be deployed after the build is done via an _install_ step. In order for this install to work, you have to provide the _-DCMAKE_INSTALL_PREFIX_ and to tell CMake what you want to do by using the _install_ function. _install_ function is very versatile. It can deploy binaries, libs, entire directories or files.
[source, cmake]
install(TARGETS my_executable DESTINATION bin)
install(TARGETS my_library DESTINATION lib)
install(DIRECTORY ${PROJECT_SOURCE_DIR}/inc DESTINATION my_lib_inc)
install(FILES ${PROJECT_SOURCE_DIR}/extra/dummy.txt DESTINATION etc)

=== Install example ===
Checkout [.yellow]#commit b0360a6#
[source, bash]
git checkout b0360a6
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=install_folder ../code/hello_cmake
cmake --build . --target install

You should see that it created a folder with name _install_folder_ and inside it, deployed the files specified.

IMPORTANT: If you don't specify -DCMAKE_INSTALL_PREFIX the install step will fail.